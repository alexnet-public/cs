#!/usr/bin/env python3

# import os
# import sys
# import subprocess
# import ctypes
# import uuid
# import tarfile
# import argparse
# from ctypes import CDLL, c_char_p

# libc = CDLL("libc.so.6")

# # Константы для создания новых namespaces
# CLONE_NEWCGROUP = 0x02000000  # Cgroup namespace
# CLONE_NEWIPC = 0x08000000     # IPC namespace
# CLONE_NEWNET = 0x40000000     # Network namespace
# CLONE_NEWNS = 0x00020000      # Mount namespace
# CLONE_NEWPID = 0x20000000     # PID namespace
# CLONE_NEWUSER = 0x10000000    # User namespace
# CLONE_NEWUTS = 0x04000000     # UTS namespace

# # Константы для операции монтирования
# MS_BIND = 4096
# MS_REC = 16384
# MNT_DETACH = 2




# def execute_pivot_root(old_root):
#     """
#     Выполняет операцию pivot_root.
#     """
#     # Предполагается, что текущий каталог уже установлен в new_root
#     new_root = "."

#     if libc.syscall(155, new_root.encode('utf-8'), old_root.encode('utf-8')) != 0:
#         errno = ctypes.get_errno()
#         raise OSError(errno, f"Ошибка pivot_root: {os.strerror(errno)}")

#     # Изменение текущего рабочего каталога в корень новой файловой системы
#     os.chdir("/")

#     # Размонтирование старого корня
#     umount2 = libc.umount2
#     umount2.argtypes = [ctypes.c_char_p, ctypes.c_int]
#     if umount2(old_root.encode('utf-8'), ctypes.c_int(2)) != 0:  # 2 = MNT_DETACH
#         errno = ctypes.get_errno()
#         raise OSError(errno, f"Ошибка размонтирования {old_root}: {os.strerror(errno)}")

# def mount_bind(source, target):
#     """
#     Выполняет bind mount из source в target.
#     """
#     if libc.mount(source.encode('utf-8'), target.encode('utf-8'), ctypes.c_char_p(None), MS_BIND | MS_REC, ctypes.c_char_p(None)) != 0:
#         errno = ctypes.get_errno()
#         raise OSError(errno, f"Ошибка монтирования {source} на {target}: {os.strerror(errno)}")

# def generate_container_id(length=12):
#     """
#     Генерирует уникальный идентификатор для контейнера.
#     Возвращает строку заданной длины (по умолчанию 12 символов).
#     """
#     return str(uuid.uuid4()).replace('-', '')[:length]


# def set_hostname(new_hostname):
#     """ Установка нового имени хоста в UTS namespace. """
#     libc.sethostname(new_hostname, len(new_hostname))


# def set_uid_map(pid, mapping):
#     uid_map = "/proc/"+str(pid)+"/uid_map"
#     print(uid_map)
#     #os.system('echo "0 1000 1" > '+uid_map)
#     with open(f"/proc/{pid}/uid_map", 'w') as uid_map:
#         uid_map.write(mapping)

# def set_gid_map(pid, mapping):
#     with open(f"/proc/{pid}/gid_map", 'w') as gid_map:
#         gid_map.write(mapping)

# def extract_rootfs(container_id, image_path, target_dir):
#     """
#     Распаковывает образ файловой системы из tar-архива в указанный каталог.
#     """
#     # Генерация уникального идентификатора для каталога
#     container_dir = os.path.join(target_dir, "rootfs")

#     # Создание каталога для контейнера
#     os.makedirs(container_dir, exist_ok=True)

#     # Распаковка образа в каталог контейнера
#     with tarfile.open(image_path, 'r:gz') as tar:
#         tar.extractall(path=container_dir)

#     return container_dir

# def find_image_file(image_name):
#     """
#     Находит файл образа по имени образа в указанной директории.
#     Возвращает полный путь к файлу образа или None, если файл не найден.
#     """
#     image_path = 'images'

#     if not os.path.exists(image_path):
#         print(f"Директория {image_path} не существует.")
#         return None

#     for file in os.listdir(image_path):
#         if file.startswith(image_name) and file.endswith(".tar.gz"):
#             return os.path.join(image_path, file)

#     print(f"Образ {image_name} не найден в директории {image_path}.")
#     return None

# def run(container_id, command, image_name):
#     # Находим путь к образу в image_path
#     image_file = find_image_file(image_name)
#     if not image_file:
#         print(f"Образ {image_name} не найден.")
#         return

#     # Путь к каталогу для распаковки образа
#     container_rootfs = os.path.join("containers", container_id)

#     # Распаковываем образ
#     extract_rootfs(container_id, image_file, container_rootfs)
#     print(f"Файловая система контейнера распакована в: {container_rootfs}")

#     # Создаем и запускаем namespace
#     create_new_namespace(command, container_rootfs)

# def create_new_namespace(command, container_rootfs):
#     # Создаем канал для синхронизации
#     parent_pipe, child_pipe = os.pipe()

#     pid = os.fork()
#     if pid == 0:
#         # Дочерний процесс
#         # Закрыть родительский конец канала
#         os.close(parent_pipe)

#         # Создать новый namespace
#         libc.unshare( CLONE_NEWUTS | CLONE_NEWNET | CLONE_NEWUSER | CLONE_NEWNS )
        
#         # Сигнализируем родительскому процессу
#         os.write(child_pipe, b'1')
#         os.close(child_pipe)

        
#         # Путь к вашей файловой системе и каталогу put_old
#         filesystem_path = container_rootfs+"/rootfs"
#         old_path = "old_path"

#         try:
#             mnt_roots = "/mnt/rootfs"
#             os.makedirs(mnt_roots, exist_ok=True)
#             print ("mkdir "+mnt_roots)

#             # Монтирование файловой системы
#             mount_bind(filesystem_path, mnt_roots)
#             print ("mount --bind "+filesystem_path+" "+mnt_roots)

#             # Создание каталога old_path внутри нового корня, если он ещё не создан
#             os.makedirs(mnt_roots+"/"+old_path, exist_ok=True)
#             print ("mkdir "+mnt_roots+"/"+old_path)

#             # Сменить каталог
#             os.chdir(mnt_roots)
#             print("chdir "+mnt_roots)

#             # Пример использования функции
#             try:
#                 # Выполнение pivot_root
#                 execute_pivot_root(old_path)
#                 print("execute_pivot_root "+old_path)
#             except OSError as e:
#                 print(f"Ошибка: {e}")

#             #rm old dir
#             print("rm -rf "+old_path)
#             os.system("rm -rf "+old_path)

#         except OSError as e:
#             print(f"Ошибка: {e}")

#         container_id = generate_container_id()
#         print("container_id "+container_id)
        
#         print("set_hostname")
#         set_hostname(container_id.encode('utf-8'))
        
#         # Выполнение команды в новом namespace
#         os.execvp(command[0], command)


#     else:
#         # Родительский процесс
#         # Закрыть дочерний конец канала
#         os.close(child_pipe)

#         # Ждем сигнала от дочернего процесса
#         os.read(parent_pipe, 1)
#         os.close(parent_pipe)

#         # Устанавливаем карты UID и GID
#         #print("Устанавливаем карты UID и GID")
#         set_uid_map(pid, "0 0 1")
#         set_gid_map(pid, "0 0 1")

#         # Ожидаем завершения дочернего процесса
#         os.waitpid(pid, 0)

# def main():
#     parser = argparse.ArgumentParser(description='Управление контейнерами.')
#     parser.add_argument('command', help='Команда для выполнения')  # Позиционный аргумент
#     parser.add_argument('-i', '--image_name', help='Имя образа')   # Опциональный аргумент
#     parser.add_argument('-c', '--cli', help='cli')
#     #parser.add_argument('-p', '--image_path', help='Путь к образам', default='images')  # Опциональный аргумент с значением по умолчанию

#     args = parser.parse_args()

#     # Получение абсолютного пути к исполняемому файлу скрипта
#     script_path = os.path.realpath(__file__)
#     script_dir = os.path.dirname(script_path)
#     # Изменение рабочего каталога на каталог скрипта
#     os.chdir(script_dir)
    

#     if args.command == 'run' and args.image_name:
#         container_id = generate_container_id()
#         run(container_id, [args.cli], args.image_name)

# if __name__ == "__main__":
#     main()





import os
import sys
import subprocess
import ctypes
import uuid
import tarfile
import argparse
from ctypes import CDLL, c_char_p

class CS:
    def __init__(self):
        # Загрузка библиотеки libc и определение констант
        self.libc = CDLL("libc.so.6")
        self.CLONE_NEWCGROUP = 0x02000000  # Cgroup namespace
        self.CLONE_NEWIPC = 0x08000000     # IPC namespace
        self.CLONE_NEWNET = 0x40000000     # Network namespace
        self.CLONE_NEWNS = 0x00020000      # Mount namespace
        self.CLONE_NEWPID = 0x20000000     # PID namespace
        self.CLONE_NEWUSER = 0x10000000    # User namespace
        self.CLONE_NEWUTS = 0x04000000     # UTS namespace

        self.MS_BIND = 4096
        self.MS_REC = 16384
        self.MNT_DETACH = 2

    def execute_pivot_root(self, old_root):
        """
        Выполняет операцию pivot_root.
        """
        new_root = "."
        if self.libc.syscall(155, new_root.encode('utf-8'), old_root.encode('utf-8')) != 0:
            errno = ctypes.get_errno()
            raise OSError(errno, f"Ошибка pivot_root: {os.strerror(errno)}")

        os.chdir("/")

        umount2 = self.libc.umount2
        umount2.argtypes = [ctypes.c_char_p, ctypes.c_int]
        if umount2(old_root.encode('utf-8'), ctypes.c_int(2)) != 0:
            errno = ctypes.get_errno()
            raise OSError(errno, f"Ошибка размонтирования {old_root}: {os.strerror(errno)}")

    def mount_bind(self, source, target):
        """
        Выполняет bind mount из source в target.
        """
        if self.libc.mount(source.encode('utf-8'), target.encode('utf-8'), ctypes.c_char_p(None), self.MS_BIND | self.MS_REC, ctypes.c_char_p(None)) != 0:
            errno = ctypes.get_errno()
            raise OSError(errno, f"Ошибка монтирования {source} на {target}: {os.strerror(errno)}")

    def generate_container_id(self, length=12):
        """
        Генерирует уникальный идентификатор для контейнера.
        """
        return str(uuid.uuid4()).replace('-', '')[:length]

    def set_hostname(self, new_hostname):
        """
        Установка нового имени хоста в UTS namespace.
        """
        self.libc.sethostname(new_hostname, len(new_hostname))

    def set_uid_map(self, pid, mapping):
        """
        Устанавливает карту UID.
        """
        uid_map = f"/proc/{pid}/uid_map"
        with open(uid_map, 'w') as uid_map_file:
            uid_map_file.write(mapping)

    def set_gid_map(self, pid, mapping):
        """
        Устанавливает карту GID.
        """
        with open(f"/proc/{pid}/gid_map", 'w') as gid_map:
            gid_map.write(mapping)

    def extract_rootfs(self, image_path, target_dir):
        """
        Распаковывает образ файловой системы из tar-архива в указанный каталог.
        """
        container_dir = os.path.join(target_dir, 'rootfs')
        os.makedirs(container_dir, exist_ok=True)

        with tarfile.open(image_path, 'r:gz') as tar:
            tar.extractall(path=container_dir)

        return container_dir

    def find_image_file(self, image_name):
        """
        Находит файл образа по имени образа.
        """
        image_path = 'images'
        if not os.path.exists(image_path):
            return None

        for file in os.listdir(image_path):
            if file.startswith(image_name) and file.endswith(".tar.gz"):
                return os.path.join(image_path, file)

        return None



    def run(self, container_id, command, image_name):

        """
        Запускает контейнер.
        """
        # Находим путь к образу в image_path
        image_file = self.find_image_file(image_name)
        if not image_file:
            print(f"Образ {image_name} не найден.")
            return

        # Путь к каталогу для распаковки образа
        container_rootfs = os.path.join("containers", container_id)

        # Распаковываем образ
        self.extract_rootfs(image_file, container_rootfs)
        print(f"Файловая система контейнера распакована в: {container_rootfs}")

        # Создаем и запускаем namespace
        self.create_new_namespace(container_id, command, container_rootfs)

    def create_new_namespace(self, container_id, command, container_rootfs):
        # Создаем канал для синхронизации
        parent_pipe, child_pipe = os.pipe()

        pid = os.fork()
        if pid == 0:
            # Дочерний процесс
            # Закрыть родительский конец канала
            os.close(parent_pipe)

            # Создать новый namespace
            self.libc.unshare( self.CLONE_NEWUTS | self.CLONE_NEWNET | self.CLONE_NEWUSER | self.CLONE_NEWNS )

            # Сигнализируем родительскому процессу
            os.write(child_pipe, b'1')
            os.close(child_pipe)


            # Путь к вашей файловой системе и каталогу put_old
            filesystem_path = container_rootfs+"/rootfs"
            old_path = "old_path"

            try:
                mnt_roots = "/mnt/rootfs"
                os.makedirs(mnt_roots, exist_ok=True)
                print ("mkdir "+mnt_roots)

                # Монтирование файловой системы
                self.mount_bind(filesystem_path, mnt_roots)
                print ("mount --bind "+filesystem_path+" "+mnt_roots)

                # Создание каталога old_path внутри нового корня, если он ещё не создан
                os.makedirs(mnt_roots+"/"+old_path, exist_ok=True)
                print ("mkdir "+mnt_roots+"/"+old_path)

                # Сменить каталог
                os.chdir(mnt_roots)
                print("chdir "+mnt_roots)

                # Пример использования функции
                try:
                    # Выполнение pivot_root
                    self.execute_pivot_root(old_path)
                    print("execute_pivot_root "+old_path)
                except OSError as e:
                    print(f"Ошибка: {e}")

                #rm old dir
                print("rm -rf "+old_path)
                os.system("rm -rf "+old_path)

            except OSError as e:
                print(f"Ошибка: {e}")

            #container_id = generate_container_id()
            print("container_id "+container_id)

            print("set_hostname")
            self.set_hostname(container_id.encode('utf-8'))

            # Выполнение команды в новом namespace
            os.execvp(command[0], command)
        else:
            # Родительский процесс
            # Закрыть дочерний конец канала
            os.close(child_pipe)

            # Ждем сигнала от дочернего процесса
            os.read(parent_pipe, 1)
            os.close(parent_pipe)

            # Устанавливаем карты UID и GID
            #print("Устанавливаем карты UID и GID")
            self.set_uid_map(pid, "0 0 1")
            self.set_gid_map(pid, "0 0 1")

            # Ожидаем завершения дочернего процесса
            os.waitpid(pid, 0)

def main():
    cs = CS()
    parser = argparse.ArgumentParser(description='Управление контейнерами.')
    parser.add_argument('command', help='Команда для выполнения')  # Позиционный аргумент
    parser.add_argument('-i', '--image_name', help='Имя образа')   # Опциональный аргумент
    parser.add_argument('-c', '--cli', help='cli')
    #parser.add_argument('-p', '--image_path', help='Путь к образам', default='images')  # Опциональный аргумент с значением по умолчанию

    args = parser.parse_args()

    # Получение абсолютного пути к исполняемому файлу скрипта
    script_path = os.path.realpath(__file__)
    script_dir = os.path.dirname(script_path)
    # Изменение рабочего каталога на каталог скрипта
    os.chdir(script_dir)
    

    if args.command == 'run' and args.image_name:
        container_id = cs.generate_container_id()
        cs.run(container_id, [args.cli], args.image_name)

if __name__ == "__main__":
    main()
